FROM python:3.8-alpine

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1 

RUN mkdir /cassette
COPY . /cassette

COPY requirements.txt /cassette/requirements.txt
RUN apk add --update --no-cache --virtual .tmp gcc libc-dev linux-headers
RUN pip install -r /cassette/requirements.txt
RUN pip install gunicorn
RUN apk  del .tmp

WORKDIR /cassette

RUN mkdir -p /data/web/media
RUN mkdir -p /data/web/staticfiles

RUN adduser -D cassette
RUN chown -R cassette:cassette /data
RUN chmod -R 755 /data/web

USER cassette

CMD ["/cassette/entrypoint.sh"]