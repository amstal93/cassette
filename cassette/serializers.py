from rest_framework.serializers import ModelSerializer
from .models import Station


class StationListSerializer(ModelSerializer):
    """ Stations List Serializer """

    class Meta:
        model = Station
        fields = ('id', 'genre', 'url', 'howl')
