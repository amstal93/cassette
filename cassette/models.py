from django.db import models


class Playlist(models.Model):
    folder = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.folder


class SongFile(models.Model):
    playlist = models.ForeignKey(
        Playlist, on_delete=models.CASCADE)

    file = models.FileField(null=True, blank=True)


class Station(models.Model):
    genre = models.CharField(max_length=100, unique=True)
    url = models.CharField(max_length=200, unique=True)
    folder = models.CharField(max_length=50, unique=True, null=False)
    # playlist = models.ForeignKey(Playlist, on_delete=models.CASCADE)
    howl = models.CharField(max_length=300, null=True, blank=True)

    def __str__(self):
        return self.genre
